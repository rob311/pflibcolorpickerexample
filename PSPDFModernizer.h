#import <Foundation/Foundation.h>
#include <substrate.h>
 
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000
 
@interface NSString (PSPDFModernizer)
 
// Added in iOS 8, retrofitted for iOS 7
- (BOOL)containsString:(NSString *)aString;
 
@end
 
#endif