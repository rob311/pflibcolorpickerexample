//
//  PFLibColorPickerExamplePrefsListController.m
//  PFLibColorPickerExamplePrefs
//
//  Created by rob311 on 06.07.2015.
//  Copyright (c) 2015 rob311. All rights reserved.
//

#import "PFLibColorPickerExamplePrefsListController.h"
#import "../libcolorpicker.h" // local since it's in or proj folder

@implementation PFLibColorPickerExamplePrefsListController

- (id)specifiers {
	if (_specifiers == nil) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"PFLibColorPickerExamplePrefs" target:self] retain];
  NSMutableArray *specifiersArray = [[NSMutableArray alloc] init];

	for (int i = 0; i < _specifiers.count; i++)
	{

		if(i == _specifiers.count - 1)
	    {
	      [specifiersArray addObject:[self footer]];
	      [specifiersArray addObject:[_specifiers objectAtIndex:i]];
	    }
	    else
	    {
	      [specifiersArray addObject:[_specifiers objectAtIndex:i]];
	    }
	}

	_specifiers = specifiersArray;

	}

	return _specifiers;
}

- (void)viewWillAppear:(BOOL)animated
{
	[self clearCache]; // old
	[self reload];// old
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{


	[super viewDidAppear:animated];
}

- (void)alertUILabelText
{
		PFColorAlert *alert = [PFColorAlert new]; // init WILL GET RELEASED ON CLOSE

		NSMutableDictionary *prefsDict = [NSMutableDictionary dictionaryWithContentsOfFile:@"/User/Library/Preferences/com.rob311.pflibcolorpickerexample.plist"];
		if (!prefsDict) prefsDict = [NSMutableDictionary dictionary];

			UIColor *startColor = LCPParseColorString([prefsDict objectForKey:@"aColor"], @"#F98D20"); // this color will be used at startup
			// show alert                               // Show alpha slider? // Code to run after close
			[alert showWithStartColor:startColor showAlpha:YES completion:
			^void (UIColor *pickedColor){
					// save pickedColor or do something with it
					NSString *hexString = [UIColor hexFromColor:pickedColor];
					hexString = [hexString stringByAppendingFormat:@":%g", pickedColor.alpha]; //if you want to use alpha
					//                                                                                                                              ^^ parse fallback to ^red
					// save hexString to your plist if desired


					[prefsDict setObject:hexString forKey:@"aColor"];

					[prefsDict writeToFile:@"/User/Library/Preferences/com.rob311.pflibcolorpickerexample.plist" atomically:YES];
			}];
}

- (void)alertUILabelTextShadow
{
	PFColorAlert *alert = [PFColorAlert new]; // init WILL GET RELEASED ON CLOSE

	NSMutableDictionary *prefsDict = [NSMutableDictionary dictionaryWithContentsOfFile:@"/User/Library/Preferences/com.rob311.pflibcolorpickerexample.plist"];
	if (!prefsDict) prefsDict = [NSMutableDictionary dictionary];

    UIColor *startColor = LCPParseColorString([prefsDict objectForKey:@"aColor2"], @"#FFFFFF"); // this color will be used at startup
    // show alert                               // Show alpha slider? // Code to run after close
    [alert showWithStartColor:startColor showAlpha:YES completion:
    ^void (UIColor *pickedColor){
        // save pickedColor or do something with it
        NSString *hexString = [UIColor hexFromColor:pickedColor];
				hexString = [hexString stringByAppendingFormat:@":%g", pickedColor.alpha]; //if you want to use alpha
        //                                                                                                                              ^^ parse fallback to ^red
        // save hexString to your plist if desired


				[prefsDict setObject:hexString forKey:@"aColor2"];

				[prefsDict writeToFile:@"/User/Library/Preferences/com.rob311.pflibcolorpickerexample.plist" atomically:YES];
    }];
}

-(PSSpecifier *)footer
{
    PSSpecifier *footer = [PSSpecifier preferenceSpecifierNamed:@"" target:self set:nil get:nil detail:nil cell:PSGroupCell edit:nil];
    [footer setProperty:[NSString stringWithFormat:@"v: %@\n© rob311 %@", [self getVersion],[self dynamicYear]] forKey:@"footerText"];
    [footer setProperty:@"1" forKey:@"footerAlignment"];

    return footer;
}

-(NSString *)getVersion
{
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:[NSArray arrayWithObjects: @"-c", @"dpkg -s com.rob311.pflibcolorpickerexample | grep 'Version'", nil]];
    NSPipe *pipe = [NSPipe pipe];
    [task setStandardOutput:pipe];
    [task launch];

    NSData *data = [[[task standardOutput] fileHandleForReading] readDataToEndOfFile];
    NSString *version = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    version = [version stringByReplacingOccurrencesOfString:@"Version: " withString:@""];
    version = [version stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    return version;
}
- (NSString *)dynamicYear
{
    NSString *dynamicYear = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:date];
    if ([yearMade isEqual:dateString]) dynamicYear = dateString;
    else dynamicYear = [NSString stringWithFormat:@"%@ - %@", yearMade, dateString];
    //[dateFormatter release];
    return dynamicYear;
}

- (void)twitter {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://user?screen_name=Rob311Apps"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=Rob311Apps"]];
    } else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:///user_profile/Rob311Apps"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/Rob311Apps"]];
    }  else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/Rob311Apps"]];
    }
}

- (void)bailey_twitter {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://user?screen_name=baileyseymour_"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=baileyseymour_"]];
    } else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:///user_profile/baileyseymour_"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/baileyseymour_"]];
    }  else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/baileyseymour_"]];
    }
}

- (void)twitter_PixelFireDev {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://user?screen_name=PixelFireDev"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=PixelFireDev"]];
    } else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:///user_profile/PixelFireDev"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/PixelFireDev"]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/PixelFireDev"]];
    }
}

-(void)bitbucket {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://bitbucket.org/pixelfiredev/libcolorpicker/"]];
}

-(void)buildmerepo {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://bitbucket.org/rob311/pflibcolorpickerexample"]];
}
@end
