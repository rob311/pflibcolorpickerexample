//
//  PFLibColorPickerExamplePrefsListController.h
//  PFLibColorPickerExamplePrefs
//
//  Created by rob311 on 06.07.2015.
//  Copyright (c) 2015 rob311. All rights reserved.
//

#import "Preferences/PSListController.h"
#import "Preferences/PSSpecifier.h"
#import "Preferences/NSTask.h"

#define yearMade @"2015"
@interface PFLibColorPickerExamplePrefsListController : PSListController

@end
